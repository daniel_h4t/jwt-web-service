package com.assessment.app.service;

import com.assessment.app.model.User;
import com.assessment.app.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Save user in db
     * @param user to register
     */
    public void registerUser(User user) {
        user.setPassword(encodePassword(user.getPassword()));
        userRepository.save(user);
    }

    /**
     * Secure password
     * @param password to encode using BCrypt
     */
    private String encodePassword(String password) {
        return new BCryptPasswordEncoder().encode(password);
    }

}