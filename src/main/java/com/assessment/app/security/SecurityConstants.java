package com.assessment.app.security;

public class SecurityConstants {
    public static final String SECRET = "JWTAssessmentApp";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final long EXPIRATION_TIME = 259_200_000;
    public static final String REGISTER_URL = "/register";
    public static final String LOGIN_URL = "/login";
}