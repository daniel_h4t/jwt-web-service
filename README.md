# JWT WEB SERVICE


## API Endpoints

### POST
`serverip:serverport` [/login]
Send data in Body of request
```javascript
{
  "username" : string,
  "password": string
}
```
`serverip:serverport` [/register]
Send data in Body of request
/register
```javascript
{
  "username" : string,
  "password": string
}
```

**Parameters**

|          Name | Required |  Type   | Description                                                                                                                                                           |
| -------------:|:--------:|:-------:| ---------------------------- |
|     `username` | required | string  | The user's email address.                                                                     |
|     `password` | required | string  | The user's password.                                                                     |

**Response**
JWT Token in Header

#### Technology
Spring Boot Web and Security, 
H2 Embedded database,
Gradle 